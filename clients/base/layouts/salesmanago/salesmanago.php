<?php

$viewdefs['base']['layout']['salesmanago'] = [
    'type' => 'simple',
    'components' => [
        [
            'view' => 'salesmanago',
            'primary' => true
        ],
    ]
];
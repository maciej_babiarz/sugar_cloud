<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once "custom/modules/Salesmanago/controller/SalesManagoController.php";
require_once "custom/modules/Salesmanago/provider/RequestProvider.php";
require_once "custom/modules/Salesmanago/model/SugarModel.php";
require_once "custom/modules/Salesmanago/model/UserModel.php";

class SmRestApi extends SugarApi
{
    public function registerApiRest()
    {
        return [
            'smSetConfiguration' => [
                'reqType' => 'POST',
                'noLoginRequired' => true,
                'path' => [
                    'salesmanago', 'configuration'
                ],
                'method' => 'setConfiguration',
                'shortHelp' => 'SALESmanago configuration update',
                'longHelp' => 'custom/clients/base/api/help/smSetConfiguration_help.html'
            ],
            'smGetConfiguration' => [
                'reqType' => 'GET',
                'noLoginRequired' => true,
                'path' => [
                    'salesmanago', 'configuration'
                ],
                'method' => 'getConfiguration',
                'shortHelp' => 'SALESmanago configuration get',
                'longHelp' => 'custom/clients/base/api/help/smGetConfiguration_help.html'
            ],
            'smDataExport' => [
                'reqType' => 'GET',
                'noLoginRequired' => true,
                'path' => [
                    'salesmanago', 'export'
                ],
                'method' => 'export',
                'shortHelp' => 'SALESmanago contacts export',
                'longHelp' => 'custom/clients/base/api/help/smDataExport_help.html'
            ],
            'smOptIn' => [
                'reqType' => 'GET',
                'noLoginRequired' => true,
                'path' => [
                    'salesmanago', 'optin'
                ],
                'method' => 'optinCallback',
                'shortHelp' => 'SALESmanago optin callback',
                'longHelp' => 'custom/clients/base/api/help/smOptIn_help.html'
            ],
            'smOptOut' => [
                'reqType' => 'GET',
                'noLoginRequired' => true,
                'path' => [
                    'salesmanago', 'optout'
                ],
                'method' => 'optoutCallback',
                'shortHelp' => 'SALESmanago optout callback',
                'longHelp' => 'custom/clients/base/api/help/smOptOut_help.html'
            ]

        ];
    }

    public function setConfiguration($api, $args)
    {
        return UserModel::setConfiguration($args);
    }

    public function getConfiguration($api, $args)
    {
        return UserModel::getConfiguration();
    }

    public function export($api, $args)
    {
        $controller = new SalesManagoController(
            UserModel::getConfiguration(),
            new RequestProvider(),
            new SugarModel()
        );
        return $controller->exportSugarContacts();
    }

    public function optinCallback($api, $args)
    {
        $controller = new SalesManagoController(
            UserModel::getConfiguration(),
            new RequestProvider(),
            new SugarModel()
        );
        $controller->smCallback('0', $args);
    }

    public function optoutCallback($api, $args)
    {
        $controller = new SalesManagoController(
            UserModel::getConfiguration(),
            new RequestProvider(),
            new SugarModel()
        );
        $controller->smCallback('1', $args);
    }
}
<?php

global $sugar_version;

//@formatter:off

$manifest = [
    'key' => 'SALESmanago_cloud',
    'acceptable_sugar_flavors' => [
        'PRO',
        'ENT',
        'ULT',
        'CE'
    ],
    'acceptable_sugar_versions' => [
        'regex_matches' => [
            '7.9.(.*?).*',
            '7.11.(.*?).*',
            '8.0.(.*?).*',
        ]
    ],
    'description'      => 'SugarCRM data synchronization with SALESmanago Marketing Automation',
    'name'             => 'SALESmanago Integration',
    'icon'             => 'logo_mini.png',
    'author'           => 'SALESmanago',
    'published_date'   => '2018-05-21',
    'readme'           => 'README.txt',
    'version'          => 'v1.0.0',
    'type'             => 'module',
    'is_uninstallable' => true,
];

//@formatter:on

$installdefs = [
    'id' => 'SALESmanago_integration',
    'language' => [
        [
            'from' => '<basepath>/Files/Language/Users/en_us.lang.php',
            'to_module' => "Users",
            'language' => 'en_us'
        ]
    ],
    'custom_fields' => [
        [
            'name' => 'sm_client_id',
            'label' => 'LBL_SM_CLIENT_ID',
            'type' => 'varchar',
            'module' => 'Users',
            'help' => 'SALESmanago integration config',
            'comment' => 'salesmanago client id',
            'default_value' => "1",
            'max_size' => 255,
            'required' => false,
            'reportable' => true,
            'audited' => false,
            'importable' => 'true',
            'duplicate_merge' => false,
        ],
        [
            'name' => 'sm_endpoint',
            'label' => 'LBL_SM_ENDPOINT',
            'type' => 'varchar',
            'module' => 'Users',
            'help' => 'SALESmanago integration config',
            'comment' => 'salesmanago endpoint',
            'default_value' => "",
            'max_size' => 255,
            'required' => false,
            'reportable' => true,
            'audited' => false,
            'importable' => 'true',
            'duplicate_merge' => false,
        ],
        [
            'name' => 'sm_contact_owner_email',
            'label' => 'LBL_SM_CONTACT_OWNER_EMAIL',
            'type' => 'varchar',
            'module' => 'Users',
            'help' => 'SALESmanago integration config',
            'comment' => 'salesmanago contact owner email',
            'default_value' => "",
            'max_size' => 255,
            'required' => false,
            'reportable' => true,
            'audited' => false,
            'importable' => 'true',
            'duplicate_merge' => false,
        ],
        [
            'name' => 'sm_api_secret',
            'label' => 'LBL_SM_API_SECRET',
            'type' => 'varchar',
            'module' => 'Users',
            'help' => 'SALESmanago integration config',
            'comment' => 'salesmanago api secret',
            'default_value' => "",
            'max_size' => 255,
            'required' => false,
            'reportable' => true,
            'audited' => false,
            'importable' => 'true',
            'duplicate_merge' => false,
        ],
        [
            'name' => 'sm_api_key',
            'label' => 'LBL_SM_API_KEY',
            'type' => 'varchar',
            'module' => 'Users',
            'help' => 'SALESmanago integration config',
            'comment' => 'salesmanago api key',
            'default_value' => "",
            'max_size' => 255,
            'required' => false,
            'reportable' => true,
            'audited' => false,
            'importable' => 'true',
            'duplicate_merge' => false,
        ],
    ],
    'post_uninstall' => [
        '<basepath>/post_uninstall.php'
    ],
    'copy' => [
        [
            'from' => '<basepath>/modules',
            'to' => 'custom/modules'
        ],
        [
            'from' => '<basepath>/Extension',
            'to' => 'custom/Extension'
        ],
        [
            'from' => "<basepath>/clients",
            'to' => "custom/clients",
        ],
    ]
];
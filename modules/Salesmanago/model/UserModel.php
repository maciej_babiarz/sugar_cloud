<?php

class UserModel
{
    public static function setConfiguration($data)
    {
        $query = "INSERT INTO users_cstm
                  (id_c,
                   sm_contact_owner_email_c,
                   sm_api_secret_c,
                   sm_api_key_c, 
                   sm_client_id_c,
                   sm_endpoint_c)
                  VALUES  
                  (:id,
                   :contactOwnerEmail,
                   :apiSecret,
                   :apiKey,
                   :clientId,
                   :endPoint)
                  ON DUPLICATE KEY UPDATE 
                   sm_contact_owner_email_c = :contactOwnerEmail,
                   sm_api_key_c = :apiKey, 
                   sm_api_secret_c = :apiSecret,
                   sm_client_id_c = :clientId,
                   sm_endpoint_c = :endPoint";

        $conn = $GLOBALS['db']->getConnection();
        $stmt = $conn->executeQuery(
            $query,
            [
                'id' => 1,
                'contactOwnerEmail' => $data['owner'],
                'clientId' => $data['clientId'],
                'endPoint' => $data['endPoint'],
                'apiSecret' => $data['apiSecret'],
                'apiKey' => $data['apiKey']
            ]
        );
        return $stmt;
    }

    public static function getConfiguration()
    {
        $sugarQuery = new SugarQuery();
        $sugarQuery->select(
            'sm_contact_owner_email_c',
            'sm_api_secret_c',
            'sm_client_id_c',
            'sm_endpoint_c',
            'sm_api_key_c'
        );
        $sugarQuery->where()->contains('id_c', 1);
        $sugarQuery->from(BeanFactory::getBean("Users"),
            [
                'team_security' => false,
                'add_deleted' => false
            ]);

        $sugarQuery->distinct(true);
        $preparedStmt = $sugarQuery->compile();
        $result = $preparedStmt->execute()->fetchAll();

        if (count($result) > 0) {
            foreach ($result['0'] as $key => $value) {
                $data[$key] = $value;
            }
            return $data;
        }
        return false;
    }
}
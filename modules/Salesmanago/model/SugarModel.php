<?php

class SugarModel
{
    public function afterContactSaveContactPrepare($bean)
    {
        if (!empty($bean->phone_mobile)) {
            $phone = $bean->phone_mobile;
        } elseif (!empty($bean->phone_work)) {
            $phone = $bean->phone_work;
        } else {
            $phone = null;
        }

        return $user = [
            'contact' => [
                'email' => $bean->email1,
                'name' => $bean->salutation . " " . $bean->first_name . " " . $bean->last_name,
                'fax' => $bean->phone_fax,
                'phone' => $phone,
                'externalId' => $bean->id,
                'address' => [
                    'streetAddress' => $bean->primary_address_street,
                    'zipCode' => $bean->primary_address_postalcode,
                    'city' => $bean->primary_address_city,
                    'country' => $bean->primary_address_country,
                ]
            ],
            'forceOptIn' => $forceOptIn = ($bean->email_opt_out == 1) ? false : true,
            'forceOptOut' => !$forceOptIn,
            'forcePhoneOptIn' => $forcePhoneOptIn = ($bean->do_not_call == 1) ? false : true,
            'forcePhoneOptOut' => !$forcePhoneOptIn,
            'properties' => [
                'account_name' => $bean->account_name,
                'title' => $bean->title,
                'description' => $bean->description,
                'assigned_to' => $bean->assigned_user_name,
                'department' => $bean->department,
                'lead_source' => $bean->lead_source
            ]
        ];
    }

    public function countContactsForExport($limit)
    {
        $sugarQuery = new SugarQuery();
        $sugarQuery->select('id');
        $sugarQuery->from(BeanFactory::getBean("Contacts"),
            [
                'team_security' => false,
                'deleted' => false
            ]);

        $sugarQuery->distinct(true);
        $preparedStmt = $sugarQuery->compile();
        $result = $preparedStmt->execute()->fetchAll();
        return ceil(count($result) / $limit);
    }

    public function getContactsBatchForExport($offset, $limit)
    {
        $sugarQuery = new SugarQuery();
        $sugarQuery->select('id');
        $sugarQuery->from(BeanFactory::getBean("Contacts"),
            [
                'team_security' => false,
                'add_deleted' => false
            ]);

        $sugarQuery->distinct(true);
        $sugarQuery->offset($offset)->limit($limit);
        $preparedStmt = $sugarQuery->compile();
        $result = $preparedStmt->execute()->fetchAll();
        return $result;
    }

    public function prepareContactForBatchExport($contact)
    {
        $bean = BeanFactory::getBean("Contacts", $contact['id'],
            [
                'disable_row_level_security' => true
            ]);

        if (!empty($bean->phone_mobile)) {
            $phone = $bean->phone_mobile;
        } elseif (!empty($bean->phone_work)) {
            $phone = $bean->phone_work;
        } else {
            $phone = null;
        }

        return [
            'contact' => [
                'email' => $bean->email1,
                'name' => $bean->salutation . ' ' . $bean->first_name . ' ' . $bean->last_name,
                'fax' => $bean->phone_fax,
                'phone' => $phone,
                'externalId' => $bean->id,
                'address' => [
                    'streetAddress' => $bean->primary_address_street,
                    'zipCode' => $bean->primary_address_postalcode,
                    'city' => $bean->primary_address_city,
                    'country' => $bean->primary_address_country,
                ]
            ],
            'forceOptIn' => $forceOptIn = ($bean->email_opt_out == 1) ? false : true,
            'forceOptOut' => !$forceOptIn,
            'forcePhoneOptIn' => $forcePhoneOptIn = ($bean->do_not_call == 1) ? false : true,
            'forcePhoneOptOut' => !$forcePhoneOptIn,
            'properties' => [
                'account_name' => $bean->account_name,
                'title' => $bean->title,
                'description' => $bean->description,
                'assigned_to' => $bean->assigned_user_name,
                'department' => $bean->department,
                'lead_source' => $bean->lead_source
            ],
        ];
    }

    public function getPrimaryEmailForContact($emails)
    {
        foreach ($emails as $email) {
            if ($email['primary_address'] == 1) {
                $primaryEmail = $email['email_address'];
            }
        }
        return ['email' => $primaryEmail];
    }

    public function optCallback($callback, $email)
    {
        $query = "UPDATE email_addresses SET opt_out = ? WHERE email_address = ?";
        $conn = $GLOBALS['db']->getConnection();
        $conn->executeQuery($query, array($callback, $email));
    }
}
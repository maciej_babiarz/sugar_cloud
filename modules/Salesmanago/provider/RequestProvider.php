<?php

class RequestProvider
{
    const METHOD_UPSERT = "/api/contact/upsert",
        METHOD_DELETE = "/api/contact/delete",
        METHOD_BATCH_UPSERT = "/api/contact/batchupsert";

    public function contactUpsertRequest($settings, $data)
    {
        return self::postRequest(
            $settings['sm_endpoint_c'],
            self::METHOD_UPSERT,
            array_merge(
                $data,
                self::getApiData($settings)
            )
        );
    }

    public function contactDeleteRequest($settings, $data)
    {
        return self::postRequest(
            $settings['sm_endpoint_c'],
            self::METHOD_DELETE,
            array_merge(
                $data,
                self::getApiData($settings)
            )
        );
    }

    public function exportContacts($settings, $data)
    {
        return self::postRequest(
            $settings['sm_endpoint_c'],
            self::METHOD_BATCH_UPSERT,
            array_merge(
                $data,
                self::getApiData($settings)
            )
        );
    }

    public static function getApiData($settings)
    {
        return [
            'async' => false,
            'owner' => $settings['sm_contact_owner_email_c'],
            'clientId' => $settings['sm_client_id_c'],
            'apiKey' => $settings['sm_api_key_c'],
            'requestTime' => time(),
            'sha' => sha1(
                $settings['sm_api_key_c']
                . $settings['sm_client_id_c']
                . $settings['sm_api_secret_c']
            ),
        ];
    }

    public function postRequest($host, $method, $obj)
    {
        $data = json_encode($obj);
        $url = 'https://' . $host . $method;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array('Content-Type: application/json', 'Content-Length: ' . strlen($data))
        );
        $response = curl_exec($ch);
        curl_close($ch);

        if ($response == false) {
            $url = 'http://' . $host . $method;
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt(
                $ch,
                CURLOPT_HTTPHEADER,
                array('Content-Type: application/json', 'Content-Length: ' . strlen($data))
            );

            $response = curl_exec($ch);
            curl_close($ch);
            return $response;
        } else {
            return $response;
        }
    }
}
<?php

class SalesManagoController
{
    public function __construct($settings, $request, $model)
    {
        $this->settings = $settings;
        $this->model = $model;
        $this->request = $request;
    }

    public function exportSugarContacts()
    {
        $batchLimit = 1000;
        $offset = 0;
        $howManyExports = $this->model->countContactsForExport($batchLimit);
        $contacts = [];

        if (!$howManyExports > 0) {
            return [
                'success' => true,
                'message' => "There are no contacts to export"
            ];
        }

        for ($k = 0; $k < $howManyExports; $k++) {
            $contactsBatch = $this->model->getContactsBatchForExport($offset, $batchLimit);
            $i = 0;

            foreach ($contactsBatch as $contact) {
                $contacts[$i] = $this->model->prepareContactForBatchExport($contact);
                $i++;
            }

            if ($i > 0) {
                $result = $this->request->exportContacts($this->settings, $contacts);
                $offset += $batchLimit;
            }
            $response[] = $result;
        }
        return $response['0'];
    }

    public function smCallback($callback, $args)
    {
        $email = $args['email'];
        $localKey = sha1($email . $this->settings['sm_api_secret_c']);
        if ($args['key'] == $localKey) {
            $this->model->optCallback($callback, $email);
        }
    }
}
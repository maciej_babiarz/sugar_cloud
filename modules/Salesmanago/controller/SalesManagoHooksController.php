<?php

class SalesManagoHooksController
{
    public function __construct($settings, $request, $model)
    {
        $this->settings = $settings;
        $this->model = $model;
        $this->request = $request;
    }

    public function afterContactSaveAction($bean)
    {
        $sugarContact = $this->model->afterContactSaveContactPrepare($bean);
        return $this->request->contactUpsertRequest(
            $this->settings,
            $sugarContact
        );
    }

    public function afterContactDeleteAction($bean)
    {
        $sugarContactEmail = $this->model->getPrimaryEmailForContact($bean->email);
        return $this->request->contactDeleteRequest(
            $this->settings,
            $sugarContactEmail
        );
    }
}
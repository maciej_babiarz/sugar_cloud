<?php

if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once "custom/modules/Salesmanago/controller/SalesManagoHooksController.php";
require_once "custom/modules/Salesmanago/provider/RequestProvider.php";
require_once "custom/modules/Salesmanago/model/SugarModel.php";
require_once "custom/modules/Salesmanago/model/UserModel.php";

class SalesmanagoLogicHooks
{
    function afterContactSave($bean, $event, $arguments)
    {
        $controller = new SalesManagoHooksController(
            UserModel::getConfiguration(),
            new RequestProvider(),
            new SugarModel()
        );
        $controller->afterContactSaveAction($bean);
    }

    function afterContactDelete($bean, $event, $arguments)
    {
        $controller = new SalesManagoHooksController(
            UserModel::getConfiguration(),
            new RequestProvider(),
            new SugarModel()
        );
        $controller->afterContactDeleteAction($bean);
    }
}

<?php

$hook_version = 1;
$hook_array = [];

$hook_array['after_delete'] = [];
$hook_array['after_delete'][] = [
    //Processing index. For sorting the array.
    1,
    //Label. A string value to identify the hook.
    'Salesmanago after contact delete method',
    //The PHP file where your class is located.
    'custom/modules/Contacts/SalesmanagoLogicHooks.php',
    //The class the method is in.
    'SalesmanagoLogicHooks',
    //The method to call.
    'afterContactDelete'
];
$hook_array['after_save'] = [];
$hook_array['after_save'][] = [
    1,
    'Salesmanago after save method',
    'custom/modules/Contacts/SalesmanagoLogicHooks.php',
    'SalesmanagoLogicHooks',
    'afterContactSave'
];

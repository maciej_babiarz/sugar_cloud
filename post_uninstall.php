<?php

$fieldsByModule = [
    'Users' => [
        'sm_client_id_c',
        'sm_endpoint_c',
        'sm_contact_owner_email_c',
        'sm_api_secret_c',
        'sm_api_key_c',
    ],
];

foreach ($fieldsByModule as $moduleName => $fields) {
    foreach ($fields as $field) {
        $dyField = new DynamicField();
        $dyField->bean = BeanFactory::getBean($moduleName);;
        $dyField->module = $moduleName;
        $dyField->deleteField($field);
    }
}

if (isset($viewdefs['base']['view']['profileactions'])) {
    foreach ($viewdefs['base']['view']['profileactions'] as $key => $profileAction) {
        //remove the link by label key
        if (in_array($profileAction['label'], ['LNK_SALESMANAGO_C'])) {
            unset($viewdefs['base']['view']['profileactions'][$key]);
        }
    }
}